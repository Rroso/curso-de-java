create table provincias(
    PRO_ID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PRO_DESCRIPCION VARCHAR(45) NOT NULL,
    PRIMARY KEY(PRO_ID)
); 
 

create table partidos(
    PAR_ID int unsigned not null auto_increment,
    PRO_ID int unsigned not null,
    PAR_DESCRIPCION varchar(45) not null, 
    primary key (PAR_ID),
    CONSTRAINT FK_partidos_provincia FOREIGN KEY FK_partidos_provincia (PRO_ID)
        references provincias (PRO_ID)
        on delete restrict
        on update restrict
); 

insert into provincias(PRO_DESCRIPCION) values ('Buenos Aires');
insert into provincias(PRO_DESCRIPCION) values ('Salta');
insert into provincias(PRO_DESCRIPCION) values ('Santa Cruz');
insert into provincias(PRO_DESCRIPCION) values ('Misiones');
insert into provincias(PRO_DESCRIPCION) values ('Entre Rios');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Lomas de Zamora');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Nordelta');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Hurlingham');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(1, 'Concordia');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Ciudad Autonoma');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Ituzaingó');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Merlo');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(2, 'Luján');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Moreno');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Olavarría');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'San Isidro');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(3, 'Florencio Varela');

insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Chivilcoy');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'La Plata');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Almirante Brown');
insert into partidos(PRO_ID, PAR_DESCRIPCION)VALUES(4, 'Lanús');


describe partidos;
DESCRIBE provincias; 

select * from provincias 
select * from partidos

delete from provincias 
where PRO_ID=1

delete from provincias 
where PRO_ID=5

delete from partidos
where PRO_ID=4
delete from provincias
where PRO_ID=4

update provincias set PRO_DESCRIPCION='bsas' where PRO_ID=1

update provincias set PRO_DESCRIPCION='buenos mates' where PRO_ID=3

update partidos set PAR_DESCRIPCION='el mejor' where PAR_ID=1

update provincias set PRO_DESCRIPCION='cba' where PRO_ID=2